﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace LearnTogether.Infrastructure
{
    public class PageData<T> : IEnumerable<T>
    {
        private IEnumerable<T> _currentItems;
        public int TotalCount { get; private set; }
        public int Page { get; private set; }
        public int PerPage { get; private set; }
        public int TotalPages { get; private set; }

        public bool HasNextPage { get;private set; }
        public bool HasPreviousPage { get; set; }

        public int NextPage
        {
            get
            {
                if(!HasNextPage)
                    throw new InvalidOperationException();
                return Page + 1;
            }
        }
        public int PreviousPage
        {
            get
            {
                if (!HasPreviousPage)
                    throw new InvalidOperationException();
                return Page - 1;
            }
        }

        public PageData(IEnumerable<T> currentItem, int totalCount  , int page, int perPage )
        {
            _currentItems = currentItem;
            TotalCount = totalCount;
            Page = page;
            PerPage = perPage;
            //Ceiling rounds up, but it return duble or decimal regarding what you are passing
            TotalPages = (int) Math.Ceiling((float) TotalCount/PerPage);
            HasNextPage = Page < TotalPages;
            HasPreviousPage = Page > 1;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _currentItems.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
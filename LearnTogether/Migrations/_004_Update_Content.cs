﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentMigrator;

namespace LearnTogether.Migrations
{
    [Migration(4)]
    public class _004_Update_Content : Migration
    {
       
            public override void Up()
            {
                Create.Column("content").OnTable("posts").AsCustom("TEXT");
            }

            public override void Down()
            {

            Delete.Column("content").FromTable("posts");
        }
       
    }
}
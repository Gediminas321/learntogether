﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentMigrator;

namespace LearnTogether.Migrations
{
    [Migration(3)]
    public class _003_Update_Content : Migration
    {

        public override void Up()
        {
            Delete.Column("content").FromTable("posts");
        }

        public override void Down()
        {


        }

    }
}
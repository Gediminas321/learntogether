﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FluentMigrator.Infrastructure;
using LearnTogether.Models;

namespace LearnTogether.Areas.Admin.ViewModels
{
    public class RoleCheckbox
    {
        public int Id { get; set; }
        public bool IsChecked { get; set; }
        public string Name { get; set; }
    }

    public class UsersIndex
    {
        public IEnumerable<User> Users { get; set; }
    }
    public class UsersNew
    {
        public IList<RoleCheckbox> Roles { get; set; }
        [Required,MaxLength(128)]
        public  string Username { get; set; }
        [Required, MaxLength(128)]
        public  string Name { get; set; }
        [Required, MaxLength(128)]
        public  string Surname { get; set; }
        [Required, MaxLength(256)]
        public  string Address { get; set; }
        [Required, MaxLength(256), DataType(DataType.EmailAddress)]
        public  string Email { get; set; }
        [DataType(DataType.PhoneNumber, ErrorMessage = "Provided phone number not valid")]
        [Display(Name = "Phone Number")]
        [Required(ErrorMessage = "Phone Number Required!")]
        [RegularExpression(@"^\s*\+?\s*([0-9][\s-]*){6,12}$", ErrorMessage = "Provided phone number not valid")]
        public  string PhoneNumber { get; set; }
        [Required]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,18}$", ErrorMessage = "Password must have uppercase letter, special character, alphanumeric characters")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [Display(Name = "Confirm password")]
        [DataType(DataType.Password)]
        [Compare("Password",
  ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class UsersEdit
    {
        public IList<RoleCheckbox> Roles { get; set; }
        [Required, MaxLength(128)]
        public string Username { get; set; }
        [Required, MaxLength(128)]
        public string Name { get; set; }
        [Required, MaxLength(128)]
        public string Surname { get; set; }
        [Required, MaxLength(256)]
        public string Address { get; set; }
        [Required, MaxLength(256), DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required, MaxLength(20), DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
    }

    public class UsersResetPassword
    {
        public string Username { get; set; }
        [Required, DataType(DataType.Password)]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,18}$", ErrorMessage = "Password must have uppercase letter, special character, alphanumeric characters")]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [Display(Name = "Confirm password")]
        [DataType(DataType.Password)]
        [Compare("Password",
  ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using LearnTogether.Areas.Admin.ViewModels;
using LearnTogether.Infrastructure;
using LearnTogether.Infrastructure.Extensions;
using LearnTogether.Models;
using NHibernate.Linq;

namespace LearnTogether.Areas.Admin.Controllers
{

    //We need a count of how many posts in the database
    // We need the post whe are on /set defoult to 1 if its not specified
    [Authorize(Roles = "admin")]
    [SelectedTab("posts")]
    public class PostsController : Controller
    {
        private const int POST_PER_PAGE = 5;
        public ActionResult Index(int page = 1)
        {
            var totalPostsCount = Database.Session.Query<Post>().Count();

            var currentPostPage = Database.Session.Query<Post>()//returns all posts
                .OrderByDescending(x => x.CreatedAt)//orders by descending order
                .Skip((page - 1) * POST_PER_PAGE)//skip pages. if page is 1 skip 0, if page 2 skip 1 and so on
                .Take(POST_PER_PAGE) //Takes top five Posts 
                .ToList();//returns top 5 in the list
            return View(new PostsIndex
            {
                Posts = new PageData<Post>(currentPostPage, totalPostsCount, page, POST_PER_PAGE)
            });
        }

        public ActionResult New()
        {
            return View("Form", new PostsForm
            {
                IsNew = true,
                Tags = Database.Session.Query<Tag>().Select(tag => new TagChekbox
                {
                    Id = tag.Id,
                    Name = tag.Name,
                    IsChecked = false
                }).ToList()
            });
        }

        public ActionResult Edit(int id)
        {
            var post = Database.Session.Load<Post>(id);
            if (post == null)
                return HttpNotFound();
            return View("Form", new PostsForm
            {
                IsNew = false,
                PostId = id,
                Title = post.Title,
                Slug = post.Slug,
                Content = post.Content,
                Tags = Database.Session.Query<Tag>().Select(tag => new TagChekbox
                {
                    Id = tag.Id,
                    Name = tag.Name,
                    IsChecked = post.Tags.Contains(tag)
                }).ToList()
            });
        }

        [HttpPost, ValidateAntiForgeryToken, ValidateInput(false)]
        public ActionResult Form(PostsForm form)
        {
            form.IsNew = form.PostId == null;
            if (!ModelState.IsValid)
                return View(form);
            Post post;
            var selectedTags = ReorganizeTag(form.Tags).ToList();
            if (form.IsNew)
            {
                post = new Post
                {
                    CreatedAt = DateTime.Now,
                    User = Auth.User

                };
                foreach (var tag in selectedTags)
                {
                    post.Tags.Add(tag);
                }
            }
            else
            {
                post = Database.Session.Load<Post>(form.PostId);
                if (post == null)
                    return HttpNotFound();

                post.UpdatedAt = DateTime.UtcNow;

                foreach (var toAdd in selectedTags.Where(x=> !post.Tags.Contains(x)))
                {
                    post.Tags.Add(toAdd);
                }
                foreach (var toRemove in post.Tags.Where(x => !selectedTags.Contains(x)).ToList())
                {
                    post.Tags.Remove(toRemove);
                }
            }
            post.Title = form.Title;
            post.Slug = form.Slug;
            post.Content = form.Content;
            Database.Session.SaveOrUpdate(post);

            return RedirectToAction("Index");
        }
        [HttpPost,ValidateAntiForgeryToken]
        public ActionResult Trash(int id)
        {
            var post = Database.Session.Load<Post>(id);
            if (post == null)
                return HttpNotFound();
            post.DeletedAt = DateTime.UtcNow;

            Database.Session.Update(post);
            return RedirectToAction("Index");
        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
           var post = Database.Session.Load<Post>(id);
            if (post == null)
                return HttpNotFound();
            Database.Session.Delete(post);
            return RedirectToAction("Index");
        }
        [HttpPost,ValidateAntiForgeryToken]
        public ActionResult Restore(int id)
        {
            var post = Database.Session.Load<Post>(id);
            if(post == null)
                return HttpNotFound();
            post.DeletedAt = null;
            Database.Session.Update(post);
            return RedirectToAction("Index");
        }

        private IEnumerable<Tag> ReorganizeTag(IEnumerable<TagChekbox> tags )
        {
            foreach (var tag in tags.Where(x=> x.IsChecked))
            {
                if (tag.Id != null)
                {
                    yield return Database.Session.Load<Tag>(tag.Id);
                    continue;
                }
                var existingTag = Database.Session.Query<Tag>().FirstOrDefault(x => x.Name == tag.Name);
                if (existingTag != null)
                {
                    yield return existingTag;
                    continue;
                }
                var newTag = new Tag()
                {
                    Name = tag.Name,
                    Slug = tag.Name.Slugify()
                };
                Database.Session.Save(newTag);
                yield return newTag;
            }
        }
    }
}
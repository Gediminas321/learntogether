﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using LearnTogether.Areas.Admin.ViewModels;
using LearnTogether.Infrastructure;
using LearnTogether.Models;
using NHibernate.Linq;

namespace LearnTogether.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    [SelectedTab("users")]
    public class UsersController : Controller
    {
        public ActionResult Index()
        {
            return View(new UsersIndex
            {
                Users = Database.Session.Query<User>().ToList()
            });
        }
        public ActionResult New()
        {
            return View(new UsersNew
            {
                Roles = Database.Session.Query<Role>().Select(role => new RoleCheckbox
                {
                    Id = role.Id,
                    IsChecked = false,
                    Name = role.RoleName
                }).ToList()
            });
        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult New(UsersNew form)
        {
            var user = new User();
            SyncRoles(form.Roles,user.Roles);

            if (Database.Session.Query<User>().Any(x => x.Username == form.Username))
            {
                ModelState.AddModelError("Username", "Username must be unique");
                return View(form);
            }

            if (Database.Session.Query<User>().Any(x => x.Email == form.Email))
            {
                ModelState.AddModelError("Email", "Email must be unique");
                return View(form);
            }


            if (!ModelState.IsValid)
                return View(form);


            user.Username = form.Username;
            user.Name = form.Name;
            user.Surname = form.Surname;
            user.Address = form.Address;
            user.PhoneNumber = form.PhoneNumber;
            user.Email = form.Email;
          
            user.SetPassword(form.Password);
            Database.Session.Save(user);

            return RedirectToAction("Index");
        }


        public ActionResult Edit(int id)
        {
            var user = Database.Session.Load<User>(id);
            if (user == null)
                return HttpNotFound();
            return View(new UsersEdit
            {
                Username = user.Username,
                Name = user.Name,
                Surname = user.Surname,
                Address = user.Address,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                Roles = Database.Session.Query<Role>().Select(role => new RoleCheckbox
                {
                    Id = role.Id,
                    IsChecked = user.Roles.Contains(role),
                    Name = role.RoleName
                }).ToList()

            });
        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Edit(int id, UsersEdit form)
        {
            var user = Database.Session.Load<User>(id);
            if (user == null)
                return HttpNotFound();

            SyncRoles(form.Roles,user.Roles);

            if (Database.Session.Query<User>().Any(x => x.Username == form.Username && x.Id != id))
            {
                ModelState.AddModelError("Username", "Username must be unique!!!");
                return View(form);
            }

            if (Database.Session.Query<User>().Any(x => x.Username == form.Username && x.Email == form.Email && x.Id != id))
                ModelState.AddModelError("Email", "Email must be unique!!!");

            if (!ModelState.IsValid)
                return View(form);


            user.Username = form.Username;
            user.Name = form.Name;
            user.Surname = form.Surname;
            user.Address = form.Address;
            user.Email = form.Email;
            user.PhoneNumber = form.PhoneNumber;

            Database.Session.Update(user);

            return RedirectToAction("Index");
        }

        public ActionResult ResetPassword(int id)
        {
            var user = Database.Session.Load<User>(id);
            if (user == null)
                return HttpNotFound();
            return View(new UsersResetPassword
            {
                Username = user.Username,
            });

        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ResetPassword(int id, UsersResetPassword form)
        {
            var user = Database.Session.Load<User>(id);
            if (user == null)
                return HttpNotFound();

            form.Username = user.Username;

            if (!ModelState.IsValid)
                return View(form);


            user.SetPassword(form.Password);
            Database.Session.Update(user);

            return RedirectToAction("index");

        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            var user = Database.Session.Load<User>(id);
            if (user == null)
                return HttpNotFound();
            Database.Session.Delete(user);
            return RedirectToAction("index");

        }
        public void SyncRoles(IList<RoleCheckbox> checkboxes, IList<Role> roles)
        {
            var selectedRoles = new List<Role>();
            foreach (var role in Database.Session.Query<Role>())
            {
                var checkbox = checkboxes.Single(x => x.Id == role.Id);
                checkbox.Name = role.RoleName;

                if(checkbox.IsChecked)
                    selectedRoles.Add(role);
            }
            //This foreach adds to the list rols that are selected
            foreach (var toAdd in selectedRoles.Where(x => !roles.Contains(x)))
            roles.Add(toAdd);
            //The foreach removes roles that are not selected, but the user have them in the database
            foreach (var toRemove in roles.Where(x => !selectedRoles.Contains(x)).ToList())
                roles.Remove(toRemove);
        }
    
    }
}
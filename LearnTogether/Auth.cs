﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LearnTogether.Models;
using NHibernate.Linq;

namespace LearnTogether
{
    public static class Auth
    {
        private const string USER_KEY = "LearnTogether.Auth.UserKey";

        public static User User
        {
            get
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    return null;
                var user = HttpContext.Current.Items[USER_KEY] as User;
                if (user == null)
                {
                    user = Database.Session.Query<User>().FirstOrDefault(x => x.Username == HttpContext.Current.User.Identity.Name);
                    if (user == null)
                        return null;
                    HttpContext.Current.Items[USER_KEY] = user;
                }
                return user;
            }
        }
    }
}
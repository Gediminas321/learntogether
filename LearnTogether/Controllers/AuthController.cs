﻿using LearnTogether.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using LearnTogether.Models;
using NHibernate.Linq;

namespace LearnTogether.Controllers
{
    public class AuthController : Controller
    {
        // GET: Auth
        public ActionResult Login()
        {
            return View(new AuthLogin {});
        }
        [HttpPost]
        public ActionResult Login(AuthLogin form, string returnUrl)
        {
            var user = Database.Session.Query<User>().FirstOrDefault(u => u.Username == form.Username);
            if (user == null)
            {
                //this code is required to fake hasching and prevent
                //the attacker to determine if the user name or email exist on the server
                LearnTogether.Models.User.FakeHash();
            }
            if(user == null || form.Password == null || !user.CheckPassword(form.Password))
                ModelState.AddModelError("Username", "Username or password is incorrect!!!");
            
            //returnUrl => model binder will automaticaly pick up return url
            if (!ModelState.IsValid)
                return View(form);

            //This is authentication
            //All encryption is handled by asp.net, so the username vill be encrypted
            FormsAuthentication.SetAuthCookie(user.Username, true);

            if (!string.IsNullOrWhiteSpace(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToRoute("home");
        }


        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToRoute("home");
        }
    }
}
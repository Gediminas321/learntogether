﻿using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using LearnTogether.Models;
using LearnTogether.ViewModels;
using NHibernate.Linq;

namespace LearnTogether.Controllers
{
    public class LayoutController : Controller
    {
        //child action only allows action to be called only from the base view
        //it cannnot be called on its own
        [ChildActionOnly]
        public ActionResult Sidebar()
        {
            return View(new LayoutSidebar
            {
                IsLogedIn = Auth.User != null,
                Username = Auth.User != null ? Auth.User.Username : "",
                IsAdmin = User.IsInRole("admin"),
                IsModerator = User.IsInRole("moderator"),
                Tags = Database.Session.Query<Tag>()
                .Select(tag => new
                {
                    tag.Id,
                    tag.Name,
                    tag.Slug,
                    PostCount = tag.Posts.Count
                }).Where(x => x.PostCount > 0)
                    .OrderByDescending(p => p.PostCount)
                    .Select(tag => new SidebarTag(tag.Id, tag.Name, tag.Slug, tag.PostCount)).ToList()
            });
        }
    }
}
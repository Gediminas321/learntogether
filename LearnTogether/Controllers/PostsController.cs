﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using LearnTogether.ViewModels;
using NHibernate.Linq;
using LearnTogether.Models;
using LearnTogether.Infrastructure;
using NHibernate.Linq.Functions;
using Remotion.Linq.Utilities;

namespace LearnTogether.Controllers
{
    public class PostsController : Controller
    {
        private const int POSTS_PER_PAGE = 5;
        public ActionResult Index(int page = 1)  
        {
            var query = Database.Session.Query<Post>()
                .Where(x => x.DeletedAt == null)
                .OrderByDescending(x => x.CreatedAt);
            var totalPosts = query.Count();
            var ids = query.Skip((page - 1) * POSTS_PER_PAGE)
                .Take(POSTS_PER_PAGE)
                .Select(x => x.Id).ToArray();
            var posts = query.Where(x => ids.Contains(x.Id))
                .FetchMany(x => x.Tags)
                .Fetch(x => x.User)
                .ToList();
            return View(new PostsIndex {
                Posts = new PageData<Post>(posts, totalPosts, page, POSTS_PER_PAGE)
            });
        }

        public ActionResult Tag(string idAndSlug, int page = 1)
        {
            var parts = Separate(idAndSlug);
            if (parts == null)
                return HttpNotFound();
            var tag = Database.Session.Load<Tag>(parts.Item1);
            if (tag == null)
                return HttpNotFound();
            if(!tag.Slug.Equals(parts.Item2, StringComparison.CurrentCultureIgnoreCase))
            return RedirectToRoutePermanent("tag", new { id = parts.Item1, slug = tag.Slug});

            var totalPosts = tag.Posts.Count;
            var postsIds = tag.Posts.Skip((page - 1) * POSTS_PER_PAGE)
                .OrderByDescending(x => x.CreatedAt)
                .Take(POSTS_PER_PAGE)
                .Where(x => x.DeletedAt == null)
                .Select(x => x.Id)
                .ToArray();

            var posts = Database.Session.Query<Post>()
                .OrderByDescending(x => x.CreatedAt)
                .Where(x => postsIds.Contains(x.Id))
                .FetchMany(x => x.Tags)
                .Fetch(x => x.User)
                .ToList();

            return View(new PostsTag
            {
                Tag = tag,
                Posts = new PageData<Post>(posts, totalPosts, page, POSTS_PER_PAGE)
            });
        }

        public ActionResult Show(string idAndSlug)
        {
            var parts = Separate(idAndSlug);
            if (parts == null)
                return HttpNotFound();

            var post = Database.Session.Load<Post>(parts.Item1);
            if (post == null || post.IsDeleted)
                return HttpNotFound();

            if (!post.Slug.Equals(parts.Item2, StringComparison.CurrentCultureIgnoreCase))
                return RedirectToRoutePermanent("Post", new {id = parts.Item1, slug = post.Slug});

            return View(new PostsShow {Post = post});
        }

        private static Tuple<int, string> Separate(string idAndSlug)
        {
            //  starting ^
            //  openn group (
            // \d+ zero to many didgits 
            //  \- escape 
            // another group any character  (.*)
            // $ end
            var matches = Regex.Match(idAndSlug, @"^(\d+)\-(.*)?$");
            if (!matches.Success)
                return null;
            var id = int.Parse(matches.Result(("$1")));
            var sluug = matches.Result("$2");
            return Tuple.Create(id, sluug);
        }
    }
}
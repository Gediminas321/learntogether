﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode.Impl;

namespace LearnTogether.Models
{
    public class User
    {
        private const int WORK_FACTOR = 13;
        public static void FakeHash()
        {
            BCrypt.Net.BCrypt.HashPassword("fake", WORK_FACTOR);
        }
    
        //every single class must be virtual in order to use hibernate object relational mapping
        public virtual int Id { get; set; }
        public virtual string Username { get; set; }
        public virtual string Name { get; set; }
        public virtual string Surname { get; set; }
        public virtual string Address { get; set; }
        public virtual string Email { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string PasswordHash { get; set; }

        public virtual IList<Role> Roles { get; set; }

        public User()
        {
            Roles = new List<Role>();
        }

        public virtual void SetPassword(string password)
        {
            PasswordHash = BCrypt.Net.BCrypt.HashPassword(password, WORK_FACTOR);
        }

        public virtual bool CheckPassword(string password)
        {
            return BCrypt.Net.BCrypt.Verify(password, PasswordHash);
        }
    }

    public class UserMap : ClassMapping<User>
    {
        public UserMap()
        {
            Table("users");
            Id(x => x.Id, x => x.Generator(Generators.Identity));
            Property(x => x.Username, x => x.NotNullable(true));
            Property(x => x.Name, x => x.NotNullable(true));
            Property(x => x.Surname, x => x.NotNullable(true));
            Property(x => x.Address, x => x.NotNullable(true));
            Property(x => x.Email, x => x.NotNullable(true));
            Property(x => x.PhoneNumber, x =>
            {
                x.Column("phone_number");
                x.NotNullable(true);
            });
            Property(x => x.PasswordHash, x =>
            {
                x.Column("password_hash");
                x.NotNullable(true);
            });
            //In the hibernate is Bag is the collection
            //it has bags, sets and lists
            //Bagts is the way to relate one or more times to the entitty 
            // List is the way to order entity in the order
            // Set is used to relate unique entities betvean each other
            Bag(x => x.Roles, x =>
            {
                x.Table("role_users");
                x.Key(k => k.Column("user_id"));
                //many to many association   
            }, x => x.ManyToMany(k => k.Column("role_id")));
        }
    }
}
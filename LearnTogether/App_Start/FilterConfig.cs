﻿using System.Web.Mvc;
using LearnTogether.Infrastructure;

namespace LearnTogether
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new TransactionFilter());
            filters.Add(new HandleErrorAttribute());
        }
    }
}
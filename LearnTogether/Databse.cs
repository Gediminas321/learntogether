﻿using System.Web;
using LearnTogether.Models;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Mapping.ByCode;


namespace LearnTogether
{
    public static class Database
    {
        private const string SESSION_KEY = "LearnTogether.Database.SessionKey";
        private static ISessionFactory _sessionFactory;
        public static ISession Session
        {
            get
            {
                return (ISession) HttpContext.Current.Items[SESSION_KEY];
            }
        }
        public static void Configure()
        {
            var configuration = new Configuration();
            //configure the connection string 
            configuration.Configure();
            //add mappings 
            var mapping = new ModelMapper();
            mapping.AddMapping<UserMap>();
            mapping.AddMapping<RoleMap>();
            mapping.AddMapping<TagMap>();
            mapping.AddMapping<PostMap>();

            configuration.AddMapping(mapping.CompileMappingForAllExplicitlyAddedEntities());
            //create session factory
            _sessionFactory = configuration.BuildSessionFactory();
        }

        public static void OpenSession()
        {
            HttpContext.Current.Items[SESSION_KEY]  = _sessionFactory.OpenSession();
        }

        public static void CloseSession()
        {
            var session = HttpContext.Current.Items[SESSION_KEY] as ISession;
            if(session != null)
           _sessionFactory.Close();
            HttpContext.Current.Items.Remove(SESSION_KEY);
        }
    }
}
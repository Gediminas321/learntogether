﻿using LearnTogether.Infrastructure;
using LearnTogether.Models;

namespace LearnTogether.ViewModels
{
    public class PostsIndex
    {
        public PageData<Post> Posts { get; set; }
    }

    public class PostsShow
    {
        public Post Post { get; set; }
    }

    public class PostsTag
    {
        public Tag Tag { get; set; }
        public PageData<Post> Posts { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LearnTogether.ViewModels
{
    //ViewModel is the contract between the controler and a view 
    public class AuthLogin
    {
        // Data that will be send from the View to the controller 
        [Required]
        public string Username { get; set; }
        [Required, DataType(DataType.Password),MinLength(5)]

        public string Password { get; set; }
    }
}
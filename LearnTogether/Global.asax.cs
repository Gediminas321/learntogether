﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using LearnTogether;
namespace LearnTogether
{
    public class MvcApplication : System.Web.HttpApplication
    {
        // Application_start will be invoked only once when application starts
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //by disabling optimizations i have solved the issue with admin/scripts/Forms.js
            // optimizations where disabled it
            BundleTable.EnableOptimizations = false;
            Database.Configure();
        }

        protected void Application_BeginRequest()
        {
            Database.OpenSession();
        }
        protected void Application_EndRequest()
        {
            Database.CloseSession();
        }
    }
}
